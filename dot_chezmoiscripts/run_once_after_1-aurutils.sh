#!/bin/bash

log(){
        echo -e "\n\033[0;32m=====>\033[0m $1"
}

log "Installing aurutils"
git clone https://aur.archlinux.org/aurutils.git ~/aurutils
cd ~/aurutils
makepkg -si

log "Creating local aur repository"
sudo cp ~/.local/share/pacman.d/aur /etc/pacman.d/

if ! grep "Include = /etc/pacman.d/aur" /etc/pacman.conf > /dev/null; then
    sudo tee -a /etc/pacman.conf << EOF

Include = /etc/pacman.d/aur
EOF
fi
sudo install -d /var/cache/pacman/aur -o $USER
repo-add /var/cache/pacman/aur/aur.db.tar
sudo pacman -Syu

log "Adding custom aur remove command"
sudo cp ~/.local/bin/aur-remove /usr/local/bin/

echo -e "\n\033[0;32maurutils installation complete\033[0m"
