#!/bin/bash

log(){
        echo -e "\n\033[0;32m=====>\033[0m $1"
}

log "Updating system"
sudo pacman -Syu

log "Installing packages"
sudo pacman -S --noconfirm --needed - < ~/packages/pkglist.txt

log "Building AUR packages"
# This method of file reading does not consume intermediate stdin calls
for pkg in $(cat ~/packages/pkglist_aur.txt); do
    aur sync --noview $pkg
done

log "Installing AUR packages"
sudo pacman -S --noconfirm --needed - < ~/packages/pkglist_aur.txt

echo -e "\n\033[0;32mPackage installation complete\033[0m"
