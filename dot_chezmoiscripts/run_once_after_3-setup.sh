#!/bin/bash

log(){
	echo -e "\n\033[0;32m=====>\033[0m $1"
}

log "Configuring Snapper"
sudo umount /.snapshots
sudo rm -rf /.snapshots
sudo snapper -c root create-config /
sudo btrfs subvolume delete /.snapshots
sudo mkdir /.snapshots
sudo mount -a
sudo chmod 750 /.snapshots

sudo systemctl enable snapper-timeline.timer
sudo systemctl enable snapper-cleanup.timer

log "Configuring UFW"
sudo systemctl enable ufw.service
sudo ufw default deny incoming
sudo ufw default allow outgoing
sudo ufw limit ssh
sudo ufw allow from 192.168.0.0/24
sudo ufw enable

log "Configuring pacman"
sudo sed -i '/Color/s/^#*//g' /etc/pacman.conf
sudo sed -i '/ParallelDownloads/s/^#*//g' /etc/pacman.conf

log "Enabling paccache timer"
sudo systemctl enable paccache.timer

log "Enabling reflector timer"
sudo systemctl enable reflector.timer

log "Enabling MPD service"
systemctl --user enable mpd
mkdir ~/.config/mpd/playlists

log "Configuring informant"
sudo gpasswd -a $USER informant

log "Creating user directories"
xdg-user-dirs-update

log "Creating Pictures/Screenshots directory"
mkdir ~/Pictures/Screenshots

log "Creating Projects directory"
mkdir ~/Projects

log "Creating repos directory"
mkdir ~/repos

log "Installing GTK theme"
sudo cp -r ~/.local/share/themes/TokyoNight /usr/share/themes/

log "Initializing rustup"
rustup default stable
rustup component add clippy
rustup component add rustfmt

log "Changing shell to zsh"
chsh -s /bin/zsh

echo -e "\n\033[0;32mSetup complete, please reboot to apply changes\033[0m"
