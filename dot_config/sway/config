# Definitions
## Keys
set {
    $mod    Mod4
    $alt    Mod1
    $left   h
    $down   j
    $up     k
    $right  l
}

## Modes
set $session_mode "session"

## Window resize factor
set $resize_factor 20px

## Applications
set {
    $term     footclient
    $devterm  alacritty
    $menu     fuzzel
}

## gsettings schema
set $gnome-interface org.gnome.desktop.interface

## Wallpaper
set $wallpaper ~/.config/wallpapers/CherryBlossomLavaWallpaper.jpg

## Colors
set {
    $black	#414868
    $blue	  #7aa2f7
    $cyan	  #7dcfff
    $night	#1a1b26
    $storm	#24283b
    $text	  #a9b1d6
    $white	#c0caf5
    $yellow	#e0af68
}


# Inputs
## Keyboard
input "type:keyboard" {
    xkb_layout  eu,us(colemak_dh)
    xkb_options grp:alt_shift_toggle,caps:escape
    repeat_rate 50
}

## Touchpad
input "type:touchpad" {
    tap             enabled
    natural_scroll  enabled
    accel_profile   "adaptive"
    pointer_accel   0.2
}

## Mouse
# $mod + left click:    Move window
# $mod + right click:   Resize window
floating_modifier $mod  normal


# Keybindings
## Windows
### Focus a window
bindsym {
    $mod+$left  focus left
    $mod+$down  focus down
    $mod+$up    focus up
    $mod+$right focus right
}

### Move focused window
bindsym {
    $mod+Shift+$left    move left
    $mod+Shift+$down    move down
    $mod+Shift+$up      move up
    $mod+Shift+$right   move right
}

### Resize windows
bindsym {
    $mod+alt+$left  resize shrink width $resize_factor
    $mod+alt+$down  resize shrink height $resize_factor
    $mod+alt+$up    resize grow height $resize_factor
    $mod+alt+$right resize grow width $resize_factor
}

### Kill focused window
bindsym $mod+q  kill

### Turn focused window fullscreen
bindsym $mod+f  fullscreen

### Make focused window sticky
bindsym $mod+p  sticky toggle

### Switch between tiling and floating modes
bindsym {
    $mod+s          floating toggle
    $mod+Shift+s    focus mode_toggle
}

### Shift focus between parent and child windows
bindsym {
    $mod+a          focus parent
    $mod+Shift+a    focus child
}

## Workspaces
### Switch to workspace
bindsym {
    $mod+1  workspace number 1
    $mod+2  workspace number 2
    $mod+3  workspace number 3
    $mod+4  workspace number 4
    $mod+5  workspace number 5
    $mod+6  workspace number 6
    $mod+7  workspace number 7
    $mod+8  workspace number 8
    $mod+9  workspace number 9
    $mod+0  workspace number 10
}

### Move focused window to workspace
bindsym {
    $mod+Shift+1    move container to workspace number 1
    $mod+Shift+2    move container to workspace number 2
    $mod+Shift+3    move container to workspace number 3
    $mod+Shift+4    move container to workspace number 4
    $mod+Shift+5    move container to workspace number 5
    $mod+Shift+6    move container to workspace number 6
    $mod+Shift+7    move container to workspace number 7
    $mod+Shift+8    move container to workspace number 8
    $mod+Shift+9    move container to workspace number 9
    $mod+Shift+0    move container to workspace number 10
}

## Layout
### Split focused window
bindsym {
    $mod+b  splith
    $mod+v  splitv
}

### Switch focused window between different layout styles
bindsym {
    $mod+t  layout tabbed
    $mod+e  layout toggle split
}

## Scratchpad
### Move focused window to the scratchpad
bindsym $mod+Shift+minus    move scratchpad

### Cycle through scratchpad windows
bindsym $mod+minus  scratchpad show

## Modes
bindsym $mod+grave	mode $session_mode
mode $session_mode bindsym {
    r       reload, mode "default"
    q       exec wlogout, mode "default"
    e       exec swaymsg exit, mode "default"
    s       exec systemctl suspend, mode "default"
    l       exec swaylock, mode "default"
    escape  mode "default"
}

### Screenshots
bindsym Print       exec grim -t png -g "$(slurp)" ~/Pictures/Screenshots/$(date +%Y%m%d_%H%M%S).png
bindsym $mod+Print  exec grim -g "$(slurp)" - | swappy -f -

### Start applications
bindsym {
    $mod+Return exec $term
    $mod+d      exec $devterm
    $mod+space  exec $menu
    $mod+w      exec $BROWSER
}


# Startup
## Start daemons
exec kanshi
exec mako
exec aa-notify -p -s 1 -w 60 -f /var/log/audit/audit.log
exec nm-applet --indicator
exec udiskie --tray
exec foot --server

## Reload daemons on Sway reload
exec_always pkill kanshi; exec kanshi

## Start status bar
bar {
    swaybar_command waybar
}

## Set idle behavior
exec swayidle -w \
    timeout 60      'swaylock' \
    #timeout 300     'swaymsg "output * dpms off"' \
    #resume          'swaymsg "output * dpms on"' \
    #timeout 600     'systemctl suspend' \
    #before-sleep    'swaylock'

## Set GTK settings
exec_always {
    gsettings set $gnome-interface cursor-theme "Adwaita"
    gsettings set $gnome-interface document-font-name "serif 12"
    gsettings set $gnome-interface font-name "sans-serif 12"
    gsettings set $gnome-interface gtk-theme "TokyoNight"
    gsettings set $gnome-interface icon-theme "Papirus-Dark"
    gsettings set $gnome-interface monospace-font-name "monospace 12"
    gsettings set $gnome-interface font-antialiasing "rgba"
    gsettings set $gnome-interface font-hinting "full"
}

## Assign applications to certain workspaces

# Appearance
## Set typeface
font pango: monospace SemiBold 12

## Set wallpaper
output * bg $wallpaper fill

## Set window borders and gaps
default_border pixel 2
default_floating_border pixel 2
gaps inner 2
gaps outer 2
smart_gaps on

## Set window decoration colors
# class                 border  backgr.	text    indic.	child_border
client.focused          $blue	$blue	$black	$cyan   $blue
client.focused_inactive $blue	$blue	$black	$cyan   $blue
client.unfocused        $black	$storm	$text	$cyan   $black
client.urgent           $yellow $yellow	$night	$cyan   $yellow


include /etc/sway/config.d/*
