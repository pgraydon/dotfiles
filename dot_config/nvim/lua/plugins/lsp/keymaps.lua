local M = {}
M._keys = nil

function M.diagnostic_goto(next, severity)
    local go = next and vim.diagnostic.goto_next or vim.diagnostic.goto_prev
    severity = severity and vim.diagnostic.severity[severity] or nil
    return function()
        go({ severity = severity })
    end
end

function M.get()
    local bufnr = vim.api.nvim_get_current_buf()

    if not M._keys then
        M._keys = {
            {
                "<leader>cr",
                vim.lsp.buf.rename,
                desc = "Rename"
            },
            {
                "<leader>ca",
                vim.lsp.buf.code_action,
                desc = "Code Action"
            },
            {
                "gd",
                function() require("telescope.builtin").lsp_definitions({ reuse_win = true }) end,
                desc = "Goto Definition"
            },
            {
                "gD",
                vim.lsp.buf.declaration,
                desc = "Goto Declaration"
            },
            {
                "gr",
                "<cmd>Telescope lsp_references<cr>",
                desc = "Goto References"
            },
            {
                "gI",
                vim.lsp.buf.implementation,
                desc = "Goto Implementation"
            },
            {
                "gy",
                vim.lsp.buf.type_definition,
                desc = "Goto Type Definition"
            },
            {
                "K",
                vim.lsp.buf.hover,
                desc = "Hover Documentation"
            },
            {
                "<C-k>",
                vim.lsp.buf.signature_help,
                desc = "Signature Documentation"
            },
            {
                "]d",
                M.diagnostic_goto(true),
                desc = "Next Diagnostic"
            },
            {
                "[d",
                M.diagnostic_goto(false),
                desc = "Prev Diagnostic"
            },
            {
                "]e",
                M.diagnostic_goto(true, "ERROR"),
                desc = "Next Error"
            },
            {
                "[e",
                M.diagnostic_goto(false, "ERROR"),
                desc = "Prev Error"
            },
            {
                "]w",
                M.diagnostic_goto(true, "WARN"),
                desc = "Next Warning"
            },
            {
                "[w",
                M.diagnostic_goto(false, "WARN"),
                desc = "Prev Warning"
            },

            -- Create a command `:Format` local to the LSP buffer
            vim.api.nvim_buf_create_user_command(bufnr, "Format", function()
                vim.lsp.buf.format()
            end, { desc = "Format current buffer with LSP" })
        }
    end

    return M._keys
end

function M.has(buffer, method)
    method = method:find("/") and method or "textDocument/" .. method
    local clients = vim.lsp.get_active_clients({ bufnr = buffer })
    for _, client in ipairs(clients) do
        if client.supports_method(method) then
            return true
        end
    end
    return false
end

function M.resolve(buffer)
    local Keys = require("lazy.core.handler.keys")
    local keymaps = {}

    local function add(keymap)
        local keys = Keys.parse(keymap)
        if keys[2] == false then
            keymaps[keys.id] = nil
        else
            keymaps[keys.id] = keys
        end
    end
    for _, keymap in ipairs(M.get()) do
        add(keymap)
    end

    local opts = require("plugins.utils.init").opts("nvim-lspconfig")
    local clients = vim.lsp.get_active_clients({ bufnr = buffer })
    for _, client in ipairs(clients) do
        local maps = opts.servers[client.name] and opts.servers[client.name].keys or {}
        for _, keymap in ipairs(maps) do
            add(keymap)
        end
    end
    return keymaps
end

function M.on_attach(client, buffer)
    local Keys = require("lazy.core.handler.keys")
    local keymaps = M.resolve(buffer)

    for _, keys in pairs(keymaps) do
        if not keys.has or M.has(buffer, keys.has) then
            local opts = Keys.opts(keys)
            opts.has = nil
            opts.silent = opts.silent ~= false
            opts.buffer = buffer
            vim.keymap.set(keys.mode or "n", keys.lhs, keys.rhs, opts)
        end
    end
end

return M
