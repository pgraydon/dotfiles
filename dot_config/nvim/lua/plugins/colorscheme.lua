return {
    {
        "folke/tokyonight.nvim",
        lazy = false,
        priority = 1000,
        opts = {
            style = "night",
            terminal_colors = true,
            styles = {
                comments = { italic = true },
                functions = { italic = true },
                keywords = {},
                variables = {},
                sidebars = "dark",
                floats = "dark",
            },
            dim_inactive = true,
        },
        config = function(_, opts)
            require("tokyonight").setup(opts)
            vim.cmd [[colorscheme tokyonight]]
        end,
    },
}
