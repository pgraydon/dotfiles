# Wayland-based Arch Linux desktop dotfiles

This repository contains the dotfiles and small scripts used to setup and configure a Wayland desktop environment based around the [Sway](https://swaywm.org/) compositor and window manager.
The color scheme used to theme the majority of the software (including GTK software) is [Tokyo Night](https://github.com/enkia/tokyo-night-vscode-theme).

Feel free to explore the repository and incorporate what you like into your own setup!

## Installation guide

`Chezmoi` is used to manage the dotfiles. After installing `chezmoi`, run this command to fetch and install the dotfiles directly: 

```
chezmoi init --apply https://gitlab.com/pgraydon/dotfiles.git
```

The documentation for `chezmoi` can be found on [the project's website](https://www.chezmoi.io).

---

Several scripts are included, which perform the necessary configurations and install the required packages, and are automatically run by chezmoi upon installation of the dotfiles.
The package installation script will then be run with every command that runs `chezmoi apply`, such as `chezmoi update`.

## Software

Here are some of the tools installed and configured by these dotfiles:

- Window manager and compositor: [Sway](https://swaywm.org/)
- Status bar: [Waybar](https://github.com/Alexays/Waybar)
- Display configuration: [kanshi](https://github.com/emersion/kanshi)
- Notifications: [mako](https://github.com/emersion/mako)
- Terminals: [foot](https://codeberg.org/dnkl/foot), [alacritty](https://alacritty.org/)
- Terminal Workspace: [Zellij](https://zellij.dev)
- Shell: [zsh](https://www.zsh.org/) with plugins, [starship](https://starship.rs)
- Application launcher: [fuzzel](https://codeberg.org/dnkl/fuzzel)
- File manager: [lf](https://github.com/gokcehan/lf)
- Editors: [neovim](https://neovim.io/), [helix](https://helix-editor.com/)
- Backups: [restic](https://restic.net/)
- Btrfs snapshots: [snapper](http://snapper.io/) with [snap-pac](https://github.com/wesbarnett/snap-pac)
- AUR helper: [aurutils](https://github.com/AladW/aurutils)
- System monitor: [btop](https://github.com/aristocratos/btop)
- Screenshots: [grim](https://github.com/emersion/grim) with [slurp](https://github.com/emersion/slurp) and [swappy](https://github.com/jtheoof/swappy)
- Web Browsers: [firefox](https://www.mozilla.org/en-US/firefox/new/), [brave](https://brave.com/)
- Music: [mpd](https://www.musicpd.org/) with [ncmpcpp](https://github.com/ncmpcpp/ncmpcpp)
